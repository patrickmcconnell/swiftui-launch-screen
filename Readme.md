#  Launch Demo

This sample app demonstrates how to create a launch screen with SwiftUI without actually creating a launch screen. For more information refer to https://pmcconnell.micro.blog/2023/05/03/creating-a-swiftui.html

The app will present a blank view that fades into an animated loading view. This way the actual SwiftUI view appears to be the launch screen.
