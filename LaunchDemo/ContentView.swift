//
//  ContentView.swift
//  LaunchDemo
//
//  Created by Patrick McConnell on 5/3/23.
//

import SwiftUI

// ContentView will display a view based on the state of our view model

struct ContentView: View {
    @EnvironmentObject var vm: ViewModel

    var body: some View {
        Group {
            switch vm.state {
            case .loading:
                LoadingView()
            case .error:
                Text("An Error Occurred...")
            case .ready:
                VStack {
                    Image(systemName: "globe")
                        .imageScale(.large)
                        .foregroundColor(.accentColor)
                    Text("Hello, world!")
                }
                .padding()
            }
        }
        .onAppear{
            Task {
                await vm.fetchData()
            }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static let vm = ViewModel()

    static var previews: some View {
        ContentView()
            .environmentObject(vm)
    }
}
