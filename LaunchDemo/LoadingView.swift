//
//  LoadingView.swift
//  LaunchDemo
//
//  Created by Patrick McConnell on 5/3/23.
//

import SwiftUI

struct LoadingView: View {
    @State var angle: CGFloat = 0

    @State private var opacity: Double = 0

    var foreverAnimation: Animation {
        Animation.linear(duration: 2.0)
            .repeatForever(autoreverses: false)
    }

    var body: some View {
        ZStack {
            Color(.systemBlue)

            VStack {
                Text("Launch Demo")
                    .font(.largeTitle)
                    .padding(.top, 64)

                Spacer()

                Image(systemName: "fish.circle.fill")
                    .resizable()
                    .scaledToFit()
                    .padding()
                    .rotationEffect(Angle(degrees: angle))
                    .animation(foreverAnimation, value: angle)
                    .onAppear {
                        self.angle = 360
                    }

                Text("Loading Some Data...")
                    .font(.headline)

                Spacer()

                Text("Copyright 2023 Squirrel Point Studios")
                    .padding(.bottom, 16)
            }
            .foregroundColor(.yellow)
            .opacity(opacity)
        }
        .ignoresSafeArea()
        .onAppear {
            let fadeAnimation = Animation.easeIn(duration: 1)

            withAnimation(fadeAnimation) {
                opacity = 1.0
            }
        }
    }
}

struct LoadingView_Previews: PreviewProvider {
    static var previews: some View {
        Group {
            LoadingView()
            LoadingView()
                .preferredColorScheme(.dark)
        }
    }
}
