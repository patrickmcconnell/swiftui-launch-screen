//
//  ViewModel.swift
//  LaunchDemo
//
//  Created by Patrick McConnell on 5/3/23.
//

import Foundation

// VMState is used to indicate the state of our ViewModel

enum VMState {
    case loading
    case error
    case ready
}

// this example view model would perform a task to prep your app to be ready,
// for example fetching some data from an API...

@MainActor
class ViewModel: ObservableObject {
    // by default the VM state is loading
    @Published var state: VMState = .loading

    public func fetchData() async {
        DispatchQueue.main.asyncAfter(deadline: .now() + 5) {
            // after our prep is complete we can set our state to ready or error, etc
            self.state = .ready
        }
    }
}
