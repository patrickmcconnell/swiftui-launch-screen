//
//  LaunchDemoApp.swift
//  LaunchDemo
//
//  Created by Patrick McConnell on 5/3/23.
//

import SwiftUI

@main
struct LaunchDemoApp: App {
    @StateObject private var vm: ViewModel

    init() {
        self._vm = StateObject(wrappedValue: ViewModel())
    }

    var body: some Scene {
        WindowGroup {
            ContentView()
                .environmentObject(vm)
        }
    }
}
